// pi
pi=3.141594;

// overlap
e=0.02;


// unit vectors
xaxis=[1,0,0];
yaxis=[0,1,0];
zaxis=[0,0,1];

// definition
fn=100;
$fn=100;

// basic diameter measured
R=126/2;
h=180;
// pitch
pitch=h/13;

// height

//h=100;

// trapezeudal thread
// width of th base
h_t=3.6;
// angle of the faces of the thread
a_t=58;
// deapth of the thread
d_t=2;

module thread()
{
	for(i=[0:h/pitch*fn-1])
		rotate(360/fn*i,zaxis)translate(pitch/fn*i*zaxis)
		render()
		hull(){
			rotate(90+atan(pitch/(2*pi*R)),xaxis)translate(R*xaxis)
				profile();
			rotate(360/fn,zaxis)translate(pitch/fn*zaxis)rotate(90+atan(pitch/(2*pi*R)),xaxis)translate(R*xaxis)
				profile();
		}
// center 
	translate(-pitch/2*zaxis)cylinder(h=h+pitch,r=R,$fn=fn);
}

module profile(){
	linear_extrude(height=e,center=true)
		polygon([
			[-e,h_t/2],
			[d_t,(h_t/2-d_t*tan(90-a_t))],
			[d_t,-(h_t/2-d_t*tan(90-a_t))],
			[-e,-h_t/2]
		]);
}
difference()
{
    thread();
    translate([0,0,-10])
        cube([200,200,20],center=true);
    translate([0,0,h+10])
        cube([200,200,20],center=true);
    cylinder(d=R*2-3, h=h+1);
}

