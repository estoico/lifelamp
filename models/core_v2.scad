// pi
pi=3.141594;
// overlap
e=0.02;

// unit vectors
xaxis=[1,0,0];
yaxis=[0,1,0];
zaxis=[0,0,1];

//definition
leds_por_vuelta = 25;
fn=leds_por_vuelta*2;
$fn=32;

C=17*(leds_por_vuelta);
// basic diameter
R=C/pi/2;//126/2;
h=180;
// pitch
pitch=h/13;

// height

//h=100;

// trapezeudal thread
// width of th base
h_t = 7;
// angle of the faces of the thread
a_t = 58;
// deapth of the thread
d_t=1.2;

module thread()
{
    difference()
    {
        translate(-pitch/2*zaxis)cylinder(h=h+pitch,r=R+d_t+0.4,$fn=fn);
        translate(-pitch/2*zaxis)cylinder(h=h+pitch,r=R,$fn=fn);
    
	for(i=[0:h/pitch*fn-1])
        // Rotate and go up
        if(i%2==0){
		rotate(360/fn*i,zaxis)translate(pitch/fn*i*zaxis)
		render()
		hull(){
            rotate(90+atan(pitch/(2*pi*R)),xaxis)translate(R*xaxis)
				flat_profile();
			rotate(360/fn/4,zaxis)
              translate(pitch/fn*zaxis)
                rotate(90+atan(pitch/(2*pi*R)),xaxis)
                  translate(R*xaxis)
				    profile();
            rotate(360/fn/4*3,zaxis)
              translate(pitch/fn*zaxis)
                rotate(90+atan(pitch/(2*pi*R)),xaxis)
                  translate(R*xaxis)
				    profile();
            rotate(360/fn,zaxis)
              translate(pitch/fn*zaxis)
                rotate(90+atan(pitch/(2*pi*R)),xaxis)
                  translate(R*xaxis)
				    flat_profile();
		}
    }
}
	//translate(-pitch/2*zaxis)cylinder(h=h+pitch,r=R,$fn=fn);
}
module flat_profile()
{
    linear_extrude(height=e, center=true)
    polygon([
      [-e, h_t/2],
      [e, h_t/2],
      [e, -h_t/2],
      [-e, -h_t/2]
    ]);
}
module profile(){
	linear_extrude(height=e,center=true)
		polygon([
			[-e,h_t/2],
			[d_t,(h_t/2-d_t*tan(90-a_t))],
			[d_t,-(h_t/2-d_t*tan(90-a_t))],
			[-e,-h_t/2]
		]);
}

//thread();
difference()
{

    thread();
    /*translate([0,0,-10])
        cube([200,200,20],center=true);
    translate([0,0,h+10])
        cube([200,200,20],center=true);
    cylinder(d=R*2-3, h=h+1);*/
}

