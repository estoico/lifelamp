# NOTICE

This project is in beta state, code needs clean-up, just finished for 
flashparty 2020

# Lifelamp

Lifelamp is a Conway's game of life implementation in a ESP32 using a
neopixel strip spiraled around a cylinder. It's controlled with almost 
any bluetooth controller thanks to a modified version of the great
Unijoysticle 2 [link] project. 

It was first presented at FLASHPARTY ONLINE 2020 [https://flashparty.dx.am/]
as a Wild compo entry. 

The full source code, instructions, part list and 3d-models are included 
for free. For licensing read LICENSE.firmware for the source code and 
LICENSE.everythingelse for.. well, everything else.

# Usage

Pair any Unijoysticle(tm) 2 compatible gamepad; move with dpad, play with the buttons 
and figure it out. It's not that difficult.

# Building one

Parts:
- ESP32 dev board (any with bluetooth)
- 60 led/meter neopixel strip of 5m/300 leds
- A 5v power supply that fits inside the cylinder and provides MORE than enough power for 
the leds of choice. A good rule is to use at least 20% more power
- Some experience wiring and some wires
- Some glue or double-sided tape
- Mounting cylinder (can be 3d printed from models or just use any cylinder of the correct diameter)


# Codebase

All source code is in firmware folder. A bootstrap.sh script is included that should generate a
full development environment in any debian-based GNU/Linux distribution. Also a Vagrantfile is 
included if you like to use managed development VMs. 

