/****************************************************************************
Based on unijoysticle 2 by riq:

http://retro.moe/unijoysticle2

Copyright 2019 Ricardo Quesada

Led lamp mutation by Esteban Torre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

#include "led_controller.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include <esp_system.h>


uni_gamepad_t gp_cache;
SemaphoreHandle_t gp_semaphore = NULL;

void controller_init()
{
	vSemaphoreCreateBinary(gp_semaphore);
}

void controller_read(const uni_gamepad_t* gp)
{
	if(xSemaphoreTake(gp_semaphore, portTICK_PERIOD_MS/2) == pdTRUE)
	{
		memcpy(&gp_cache, gp, sizeof(gp_cache));
		xSemaphoreGive(gp_semaphore);
	}
}

void get_controller_status(uni_gamepad_t* gp)
{
	if(xSemaphoreTake(gp_semaphore, portTICK_PERIOD_MS/2) == pdTRUE)
	{
		memcpy(gp, &gp_cache, sizeof(gp_cache));
		xSemaphoreGive(gp_semaphore);
	}
}
