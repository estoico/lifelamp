#ifndef _TIMING_H
#define _TIMING_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_timer.h"

typedef uint32_t milliseconds;

milliseconds IRAM_ATTR millis();

#endif /* #ifndef _TIMING_H */
