/****************************************************************************
Based on unijoysticle 2 by riq:

http://retro.moe/unijoysticle2

Copyright 2019 Ricardo Quesada

Led lamp mutation by Esteban Torre

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

// ESP32 version

#include "driver/gpio.h"
#include "esp_timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "math.h"
#include "uni_config.h"
#include "uni_debug.h"
#include "uni_hid_device.h"
#include "uni_platform.h"
#include <esp_system.h>
#include "timing.h"
#include "esp32_digital_led_lib.h"
#include "led_controller.h"
#include "life_game.h"

// TODO: --- Move out ---
#define HIGH 1
#define LOW 0
#define OUTPUT GPIO_MODE_OUTPUT
#define INPUT GPIO_MODE_INPUT

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2
#define floor(a) ((int)(a))
#define ceil(a) ((int)((int)(a) < (a) ? (a + 1) : (a)))

#define MAX(a, b)                               \
  ({                                            \
    __typeof__(a) _a = (a);                     \
    __typeof__(b) _b = (b);                     \
    _a > _b ? _a : _b;                          \
  })

#define MIN(a, b)                               \
  ({                                            \
    __typeof__(a) _a = (a);                     \
    __typeof__(b) _b = (b);                     \
    _a < _b ? _a : _b;                          \
  })

int x = 12;
int y = 6;


uint mode = 0;
typedef struct
{
	int(*led_fn)(strand_t*, uint32_t);
} led_mode;

#define MAX_MODES 4

led_mode modes[MAX_MODES];

uint32_t colors[] = {
    0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF, 0x001E014E,
    0x00E0FF00, 0x00D700FF, 0x0000E5FF, 0x0070FF00, 0x00E77700,
    0x000073FF, 0x0000FF77, 0x00C0FFB9, 0x00B044D9, 0x007077FF,
    0x0050FFB9, 0x990000FF, 0x330000FF,
};
#define N_COLORS (sizeof(colors)/sizeof(uint32_t))

uint32_t random_color()
{
	return colors[esp_random()%N_COLORS];
}


int render_leds(strand_t *strands[], int numStrands)
{
  /*  TODO: multi strands  */
	int res = modes[mode].led_fn(strands[0], millis());
	digitalLeds_drawPixels(strands, numStrands);
  return res;
}

uint32_t random_on_chance = 10;
uint32_t intensity = 0xFF;

int random_strand(strand_t *pStrand, milliseconds time)
{
//		uint32_t frame_t = millis() % 30000;
	for(int i=0; i<pStrand->numPixels;++i)
	{
		uint32_t x = esp_random()%0x34;
		if(x>intensity) x = intensity;
		uint32_t res = (intensity-x) * 0x1000000 +\
			       0x34-x * 0x10000 +\
			       (intensity-x)/4 * 0x100 +\
			       (intensity-x)/2;

		pStrand->pixels[i].num = res;
	}
	int j = x+y*25;
	pStrand->pixels[j].num = (uint32_t)0xED33557E;
  return 0;
}

uint32_t random_change_chance = 17;
uint32_t fade_color = 0xCCAACCFF;
uint32_t fade_color_t = 0;
uint32_t fade_next_color = 0xCCAACCFF;
uint32_t fade_next_color_t = 0;

inline uint8_t
get_byte(int n, uint32_t x)
{
        return (x>>(n*8)) & 0xFF;
}


int random_fade(strand_t *p_strand, milliseconds time)
{
        for (uint16_t i = 0; i < p_strand->numPixels; i++) {
                uint32_t r_n = esp_random();
                int change_n = r_n % 0xFF;
                int on_n = (r_n / 0xF0) % 0x1FF;
                if ((change_n <= random_change_chance) &&
                        (on_n <= random_on_chance))
                {
			uint32_t c = random_color();
                        p_strand->pixels[i].num = (int)(get_byte(0, c) * (intensity/255.0)) +
				(int)(get_byte(1, c) * (intensity/255.0)) * 0x100 +
				(int)(get_byte(2, c) * (intensity/255.0)) * 0x10000 +
				(int)(get_byte(3, c) * (intensity/255.0)) * 0x1000000;
                } else {
                        if (p_strand->pixels[i].num != 0) {
                                uint8_t c1 = get_byte(0, p_strand->pixels[i].num);
                                if (c1 > 0)
                                        c1--;
                                uint8_t c2 = get_byte(1, p_strand->pixels[i].num);
                                if (c2 > 0)
                                        c2--;
                                uint8_t c3 = get_byte(2, p_strand->pixels[i].num);
                                if (c3 > 0)
                                        c3--;
                                uint8_t c4 = get_byte(3, p_strand->pixels[i].num);
                                if (c4 > 0)
                                        c4--;
				p_strand->pixels[i].num = c1 + c2 * 0x100 +
                                                          c3 * 0x10000 +
                                                          c4 * 0x1000000;
                        }
                }
        }
        return 0;
}

uint32_t frame_counter = 100;
uint32_t frame_skip = 27;
int frame_change = 1;
uint32_t frame_color = 0xFF;
uint32_t speed_mod = 1;
uint32_t movit_timeout = 0;

int movit(strand_t *p_strand, milliseconds time)
{
  frame_counter += frame_change;
  if (esp_random() % 1234 >= 1212)
          frame_counter += frame_change * 3;
  if (esp_random() % 1000 >= 997)
          frame_skip = 17 + esp_random() % 97;

  if (esp_random() % 1000 >= 992){
		uint32_t c = random_color();
		frame_color = (int)(get_byte(0, c) * (intensity/255.0)) +
			(int)(get_byte(1, c) * (intensity/255.0)) * 0x100 +
			(int)(get_byte(2, c) * (intensity/255.0)) * 0x10000 +
			(int)(get_byte(3, c) * (intensity/255.0)) * 0x1000000;
	}
        if (esp_random() % 1000 >= 994) {
                do {
                        frame_change = 5 - esp_random() % 11;
                } while (frame_change == 0);
        }
        if (frame_counter > 2742) {
                frame_change = -1;
        }
        if (frame_counter < 10) {
                frame_change = 1;
        }
	for (uint16_t i = 0; i < p_strand->numPixels; i++) {
		int x = (i + frame_counter) % frame_skip == 0;
		if (x)
			p_strand->pixels[i].num = frame_color;
		else
			p_strand->pixels[i].num = 0;
	}
        if (esp_random() % 999 >= 989)
                speed_mod = esp_random() % 11;
	vTaskDelay(speed_mod);
  if(time > movit_timeout){
    movit_timeout = 0xFFFFFFFF;
    return 1;
  }else
    return 0;
}

int off(strand_t* p_strand, milliseconds time)
{
	for (uint16_t i = 0; i < p_strand->numPixels; i++) {
		if((esp_random()%0x1FF)<random_on_chance)
			p_strand->pixels[i].num = intensity + intensity%2;
		else if(p_strand->pixels[i].num > 1)
			p_strand->pixels[i].num -= 2;
	}
  return 0;
}

void gpioSetup(int gpioNum, int gpioMode, int gpioVal)
{
	gpio_num_t gpioNumNative = (gpio_num_t)gpioNum;
	gpio_mode_t gpioModeNative = (gpio_mode_t)gpioMode;
	gpio_pad_select_gpio(gpioNumNative);
	gpio_set_direction(gpioNumNative, gpioModeNative);
	gpio_set_level(gpioNumNative, gpioVal);
}

#define LED_PIN 17

strand_t STRANDS[] = {
	{ .rmtChannel = 1,
	  .gpioNum = LED_PIN,
	  .ledType = LED_SK6812W_V1,
	  .brightLimit = 255,
	  .numPixels = 300,
	  .pixels = NULL,
	  ._stateVars = NULL },
};
int STRANDCNT = sizeof(STRANDS) / sizeof(STRANDS[0]);

void setup_leds()
{
  init_game();
	modes[0].led_fn = &movit;
	modes[1].led_fn = &render_game;
	modes[2].led_fn = &off;
	modes[3].led_fn = &random_fade;
	digitalLeds_initDriver();
	gpioSetup(LED_PIN, OUTPUT, LOW);
	strand_t *strands[8];
	for (int i = 0; i < STRANDCNT; i++) {
		strands[i] = &STRANDS[i];
	}
	digitalLeds_addStrands(strands, STRANDCNT);
  movit_timeout = millis()+4333;
}


uint32_t tick_delay=1000/25;
uint32_t last_tick=0;

void run_test()
{
	uni_gamepad_t gp_cache;
	get_controller_status(&gp_cache);

	if(gp_cache.buttons&BUTTON_SHOULDER_L && intensity > 0)
		mode = (mode+1)%4;
	if(gp_cache.buttons&BUTTON_SHOULDER_R && random_on_chance > 0)
		mode = (mode-1)%4;
}

void run_leds()
{
	run_test();
	strand_t *strands[STRANDCNT];
	for (int i = 0; i < STRANDCNT; i++) {
		strands[i] = &STRANDS[i];

	}
	int res = render_leds(strands, STRANDCNT);
  if(res != 0)
    mode = (mode+1)%4;
}
// END: TODO: --- Move out ---
// --- Globals

static int64_t g_last_time_pressed_us = 0;  // in microseconds
static EventGroupHandle_t g_event_group;


// --- Code

static void joy_update_port(uni_joystick_t* joy, const gpio_num_t* gpios);

static void delay_us(uint32_t delay);

static void event_loop(void* arg);


void uni_platform_init(int argc, const char** argv) {
  UNUSED(argc);
  UNUSED(argv);

  setup_leds();
  controller_init();
  g_event_group = xEventGroupCreate();
  xTaskCreate(event_loop, "event_loop", 2048, NULL, 10, NULL);

}

// Events
void uni_platform_on_init_complete() {
  // Turn Off LEDs
}

void uni_platform_on_port_assign_changed(uni_joystick_port_t port) {
  bool port_status_a = ((port & JOYSTICK_PORT_A) != 0);
  bool port_status_b = ((port & JOYSTICK_PORT_B) != 0);
}

void uni_platform_on_mouse_data(int32_t delta_x, int32_t delta_y,
                                uint16_t buttons) {
}

void uni_platform_on_joy_a_data(uni_joystick_t* joy) {
//  joy_update_port(joy, JOY_A_PORTS);
}

void uni_platform_on_joy_b_data(uni_joystick_t* joy) {
//  joy_update_port(joy, JOY_B_PORTS);
}
void led_platform_on_gamepad(const uni_joystick_port_t port, const uni_gamepad_t* gp) {
	controller_read(gp);
}

uint8_t uni_platform_is_button_pressed() {
  return 0;
}

static void joy_update_port(uni_joystick_t* joy, const gpio_num_t* gpios) {
}


static void event_loop(void* arg) {
  while (1) {
    vTaskDelay(1);
    run_leds();
  }
}


// Delay in microseconds. Anything bigger than 1000 microseconds (1 millisecond)
// should be scheduled using vTaskDelay(), which will allow context-switch and
// allow other tasks to run.
static void delay_us(uint32_t delay) {
  if (delay > 1000)
    vTaskDelay(delay / 1000);
  else
    ets_delay_us(delay);
}


