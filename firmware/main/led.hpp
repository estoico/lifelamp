#ifndef LEDS_H
#define LEDS_H
#include <esp_system.h>
#ifdef __cplusplus
extern "C"{
void setup_leds();
void run_leds();
  uint32_t IRAM_ATTR millis();
}
#else
extern void setup_leds();
extern void run_leds();
extern uint32_t IRAM_ATTR millis();
#endif
#endif
