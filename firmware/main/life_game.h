#ifndef _LIFE_GAME_H
#define _LIFE_GAME_H
#include "esp32_digital_led_lib.h"
#include "timing.h"

void init_game();
int render_game(strand_t *pStrand, milliseconds time);

#endif /* #ifndef _LIFE_GAME_H */
