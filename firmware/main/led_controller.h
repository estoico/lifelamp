#ifndef LED_CONTOLLER_H
#define LED_CONTOLLER_H
#include "uni_hid_device.h"

void controller_init();

void controller_read(const uni_gamepad_t* gp);

void get_controller_status(uni_gamepad_t* gp);

#endif /* #ifndef LED_CONTOLLER_H */
