#include "life_game.h"
#include "timing.h"
#include "led_config.h"
#include "led_controller.h"

const milliseconds GAME_SPEED =    420;
uint32_t GAME_COLOR =       0xDF000000;

uint32_t GAME_COLORS[] = {0x0000FF, 0x008899, 0x00FF00, 0x889900, 0xFF0000, 0x880099};
int current_color = 0;

int buffer_a[NUM_LEDS];
int buffer_b[NUM_LEDS];

const int SELECT_A = 1;
const int SELECT_B = 2;
int which_buffer =   1;
int game_active =    0;

milliseconds game_tick = 0;

/*  TODO: typdef led_color uint32_t */
const uint32_t CURSOR_GLOW_MAX = 0xFF0000;
const uint32_t CURSOR_GLOW_MIN = 0x550000;
const int      CURSOR_GLOW_STEP = 0x40000;
const milliseconds CURSOR_TIMEOUT =  4321;
const milliseconds CURSOR_SPEED =     100;

typedef struct {
  int pos;
  uint32_t glow;
  int glow_vector;
  milliseconds active;
} cursor_t;

cursor_t cursor;

void init_cursor()
{
  cursor.pos = 12 + 6 * LINE_WIDTH;
  cursor.glow = CURSOR_GLOW_MAX;
  cursor.glow_vector = -CURSOR_GLOW_STEP;
  cursor.active = millis();
}

int up(int pos)
{
  return (pos - LINE_WIDTH + NUM_LEDS) % NUM_LEDS;
}

int down(int pos)
{
  return (pos + LINE_WIDTH) % NUM_LEDS;
}

int right(int pos)
{
  int mod = pos % LINE_WIDTH;
  int new = (mod - 1 + LINE_WIDTH) % LINE_WIDTH;
  return (pos-mod+new);
}

int left(int pos)
{
  int mod = pos % LINE_WIDTH;
  int new = (mod + 1) % LINE_WIDTH;
  return (pos-mod+new);
}

void tick_cursor(milliseconds time)
{
  int *buffer = which_buffer == SELECT_A? buffer_a : buffer_b;
	uni_gamepad_t gp_cache;

	get_controller_status(&gp_cache);

	if(gp_cache.buttons&BUTTON_A)
		buffer[cursor.pos] = 1;
	if(gp_cache.buttons&BUTTON_B)
		buffer[cursor.pos] = 0;
	if(gp_cache.buttons&BUTTON_X)
		game_active = 1;
	if(gp_cache.buttons&BUTTON_Y)
		game_active = 0;
  if(gp_cache.buttons&BUTTON_THUMB_L)
    GAME_COLOR = 0xDF000000;
  if(gp_cache.buttons&BUTTON_THUMB_R)
    GAME_COLOR = GAME_COLORS[current_color];

  if(gp_cache.dpad &&
     (time - cursor.active) >= CURSOR_SPEED)
  {
    cursor.active = time;
		if(gp_cache.dpad&DPAD_RIGHT)
      cursor.pos = right(cursor.pos);
		else if(gp_cache.dpad&DPAD_LEFT)
      cursor.pos = left(cursor.pos);
		if(gp_cache.dpad&DPAD_DOWN)
      cursor.pos = down(cursor.pos);
		else if(gp_cache.dpad&DPAD_UP)
      cursor.pos = up(cursor.pos);
  }
  cursor.glow += cursor.glow_vector;
  if(cursor.glow > CURSOR_GLOW_MAX){
    cursor.glow = CURSOR_GLOW_MAX;
    cursor.glow_vector = -CURSOR_GLOW_STEP;
  }else if(cursor.glow < CURSOR_GLOW_MIN){
    cursor.glow = CURSOR_GLOW_MIN;
    cursor.glow_vector = CURSOR_GLOW_STEP;
  }

}

void tick_color()
{
  if(GAME_COLOR != 0xDF000000)
  {
    int nci = (current_color+1)%(sizeof(GAME_COLORS)/sizeof(uint32_t));
    uint32_t nc = GAME_COLORS[nci];
    int pass = 0;

    int b = (GAME_COLOR&0xFF0000)/0x10000;
    int nb = (nc&0xFF0000)/0x10000;
    if(b>nb)
      b--;
    else if(b<nb)
      b++;
    else
      pass++;

    int g = (GAME_COLOR&0xFF00)/0x100;
    int ng = (nc&0xFF00)/0x100;
    if(g>ng)
      g--;
    else if(g<ng)
      g++;
    else
      pass++;

    int r = GAME_COLOR&0xFF;
    int nr = nc&0xFF;
    if(r>nr)
      r--;
    else if(r<nr)
      r++;
    else
      pass++;
    if(pass==3)
      current_color = nci;
    GAME_COLOR = b * 0x10000 + g * 0x100 + r;
  }
}

int count_neighbours(int *buffer, int pos)
{
  return buffer[up(left(pos))] + buffer[up(pos)] + buffer[up(right(pos))] +\
    buffer[left(pos)] + buffer[right(pos)] + \
    buffer[down(left(pos))] + buffer[down(pos)] + buffer[down(right(pos))];
}

void tick_game(milliseconds time)
{
  if(game_active &&
     (time-game_tick >= GAME_SPEED)){

    game_tick = time;

    int *present;
    int *future;
    /*  flip buffers */
    if(which_buffer == SELECT_A){
      present = buffer_a;
      future = buffer_b;
      which_buffer = SELECT_B;
    }else{
      present = buffer_b;
      future = buffer_a;
      which_buffer = SELECT_A;
    }
    /* calculate future */
    for(int i = 0; i < NUM_LEDS; ++i) {
      int n = count_neighbours(present, i);
      if(present[i] == 1)
        future[i] = (n == 2 || n == 3)? 1 : 0;
      else
        future[i] = (n == 3)? 1 : 0;
    }
  }
}

void init_game()
{
  init_cursor();
  game_tick = millis();
}

int render_game(strand_t *pStrand, milliseconds time)
{
  tick_cursor(time);
  tick_game(time);
  // Fast for demo
  tick_color();
  tick_color();
  tick_color();

  int *buffer = which_buffer == SELECT_A? buffer_a : buffer_b;

  for(int i = 0; i < NUM_LEDS; ++i) {
    pStrand->pixels[i].num = buffer[i]? GAME_COLOR : 0;
  }

  if((time-cursor.active) < CURSOR_TIMEOUT)
    pStrand->pixels[cursor.pos].num = pStrand->pixels[cursor.pos].num | cursor.glow;
  return 0;
}
